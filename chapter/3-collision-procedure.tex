\section{Procedure dell'attacco}

\begin{figure}[!htb]
    \centering
    \includegraphics[scale=0.5]{./images/collision-procedure.png}
    \caption{Passi principali dell'attacco}
    \label{fig:coll-proc}
\end{figure}

Come descritto in Figura \ref{fig:coll-proc}, la procedura d'attacco si suddivide in:

\begin{enumerate}
    \item Selezione del \textit{disturbance vector}
    \item Costruzione del \textit{differential path} non lineare
    \item Determinare le condizioni di attacco durante tutti gli step
    \item Condizioni aggiuntive oltre il \textit{differential path} per uno stop prematuro
    \item Risoluzione delle condizioni di attacco durante i primi step
    \item Ricerca delle regole di modifica del messaggio per accellerare la ricerca di collisioni
    \item Scrittura dell'algoritmo di attacco
\end{enumerate}

\subsection{Selezione del Disturbance Vector}

La selezione del \textit{disturbance vector} è una delle scelte più importanti siccome determina 
diversi aspetti di tutta la procedura d'attacco, incluse le differenze XOR del messaggio.

Inoltre esso determina le scelte di attacco ottimali sui \textit{linear path}, inclusi i candidati 
ottimali del \textit{non-linear path} e le equazioni ottimali dei \textit{message-bit} per
massimizzare le probabilità di successo.

\subsection{Costruzione del differential path non lineare}

Una volta fissati \textit{disturbance vector} e la corrispondete parte lineare del 
\textit{differential path}, il prossimo step è quello di trovare un \textit{differential path} 
non lineare idoneo a connettere le coppie di \textit{chaining value} alla parte lineare.

Questo step va eseguito separatamente per ogni \textit{near-collision} di tutto l'attacco in 
quanto nei primi step si hanno più gradi di libertà rispetto agli step successivi.

\subsection{Determinare le condizioni di attacco}

Dopo aver deciso il \textit{disturbance vector} ed aver costruito un path non lineare, il 
prossimo step è quello di determinare tutte le equazioni dell'attacco.

Il sistema di equazioni viene espresso interamente sulla computazione di M1 e consiste in due tipi di equazioni:

\begin{enumerate}
    \item Equazioni lineari sui bit del messaggio. Vengono usate per controllare il segno positivo delle 
        \textit{message word}. Siccome ci sono più "segni" lungo il path lineare con la stessa probabilità 
        si utilizza un sottospazio vettoriale in modo da ridurre il numero di equazioni
    \item Equazioni lineari sui bit di stato dati dal \textit{differential path} allo step \textit{i}.
        Controllano se c'è una differenza nei bit di stato e che segno ha, inoltre forzano alcune 
        differenze nell'output della funzione booleana.
\end{enumerate}

\subsection{Condizioni aggiuntive al differential path}

Come spiegato precedentemente, il sistema di equazioni consiste in equazioni lineari sui bit del messaggio e di 
equazioni lineari sui bit di stato.
Nell'algoritmo di ricerca delle collisioni dipendiamo dalle equazioni dei bit di stato per fermare la computazione 
di una soluzione non accettabile il prima possibile.
Dopo lo step 23 si avranno molti \textit{differential paths} che aumentano la probabilità di successo ma questi 
aumentano anche il numero di differenze distinte nel \textit{message word} che diminuisce il numero generale delle 
equazioni.
Per mantere alto il numero di vincoli, dallo step 21 allo step 25 vengono considerate equazioni lineari sui bit di stato 
\textbf{E} bit del messaggio.

\subsection{Risoluzione durante i primi step}

Questo passo non è strettamente necessario se ci sono sufficienti gradi di libertà nella parte non-lineare (come 
nell'attacco \textit{near-collision} del primo blocco).
Mentre nel caso del secondo blocco, siccome bisogna partire con un \textit{chaining} value completamente fissato 
presenta molte più condizioni nei primi step, per questo la costruzione di un ottimo e risolvibile 
\textit{differential path} non lineare è risultato molto complicato.


Per risolvere questo problema viene trasformato in un problema di \textit{satisfiability} (SAT) e viene utilizzato 
un SAT solver per trovare un sostituto al \textit{differential path} che sia risolvibile nei primi 8 step.

\subsection{Modifiche al messaggio per accellerare la ricerca di collisioni}

Per accellerare la ricerca delle collisioni in modo significativo è importante utilizzare delle regole di modifica 
del messaggio, in modo che vengano effettuati dei piccoli cambiamenti nel blocco del messaggio corrente che non 
vadano ad impattare i bit coinvolti nelle equazioni di stato e del messaggio nei prossimi \textit{n} step.

La prima tecnica di velocizzazione utilizzata è chiamata \textit{neutral bits}, un messaggio viene considerato 
neutrale fino allo step \textit{n} se modificando questo bit non si hanno cambiamenti che interagiscono con il 
\textit{differential path} fino allo step \textit{n}.

Un'altra tecnica utilizzata è quella del "boomerang" che consiste nel scegliere un numero di bit che modificati 
contemporaneamente modificano un solo bit di stato nei primi 16 step e quindi l'introduzione di modifiche incontrollabili 
viene ritardata notevolmente.

\subsection{Implementazione dell'attacco}

L'ultimo step rimantente è l'implementazione effettiva dell'attacco.
La prima coppia di blocchi per la \textit{near-collision} è stata computata con CPU utilizzando una versione 
modificata del software \textit{HashClash} mentre per il secondo blocco che richiede una computazione molto più intensa 
sono state usate GPU con un framework custom ideato dai ricercatori.
