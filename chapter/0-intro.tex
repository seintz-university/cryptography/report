\section{Introduzione}
La funzione crittografica SHA-1 è una funzione di hashing ampiamente usata, specialmente per il controllo di autenticità e firma dei software, nei certificati TLS o nel versioning di GIT a scopo di backup e/o integrità.

Nel caso della firma digitale, il calcolo del valore di hashing è equivalente all'apposizione della firma su un documento cartaceo e quindi garantisce che quel documento sia stato validato dal firmante. Un esempio concreto dell'utilizzo della firma digitale è dato dal rilascio di aggiornamenti o applicativi da parte di un vendor tramite dei siti di hosting non affiliati.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{images/digitalsignature.png}
    \caption{Esempio di utilizzo di una digital signature nel rilascio di software}
\end{figure}
Una volta pubblicato il software, il vendor genera e rilascia la firma utile alla verifica da parte degli utenti in seguito al download dell'applicativo.\newline
Nel caso in cui la firma non sia valida allora è possibile che qualche intermediario abbia apportato delle modifiche al software.

I sistemi di digital signature, e più in generale i sistemi che fanno uso di funzioni di hashing crittografiche, sono considerati sicuri in quanto per un avversario \texttt{A} non esiste un metodo efficiente per generare un messaggio \texttt{m} con una signature o hashing appartenente a Bob, che possa essere verificato tramite la funzione \texttt{verify} da Alice. L'affermazione rimane valida anche se \texttt{A} è in possesso di altri messaggi e signature appartenti a Bob, a patto che \texttt{m} non sia tra questi messaggi.

\subsection{Funzioni di hashing}

Una funzione di \textit{hashing} $h$ generica è una funzione matematica che rispetta tre proprietà:

\begin{enumerate}
    \item è efficiente nell'effettuare la computazione da dominio a codominio.
    \item il dominio è formato dalle stringhe di qualunque lunghezza;
    \item produce in uscita una stringa di lunghezza $l$ fissa e deterministica;
\end{enumerate}
\begin{equation}
    h: \{0,1\}^* -> \{0,1\}^l
\end{equation}
Affinchè una funzione di \textit{hashing} possa essere considerata sicura ed utilizzabile in crittografia è necessario che rispetti altre proprietà più stringenti sulla modalità di trasformazione da input ad output:

\begin{enumerate}
    \item resistenza alla preimmagine (o \textit{hiding} o \textit{strong collision}): dato un hash $h$, è infattibile risalire ad un messaggio $m$ con hash $h$ (nel caso $m$ sia in un insieme ristretto è possibile utilizzare un $nonce$ come input addizionale alla funzione: $H(nonce || m)$);
    \item resistenza alla seconda preimmagine (o \textit{weak collision}): dato un messaggio $m_1$ è infattibile trovare un messaggio $m_2$, diverso da $m_1$, tale che $hash(m_1)=hash(m_2)$.
    \item resistenza alle collisioni: dati due messaggi $m_1$ e $m_2$, diversi tra loro, deve essere infattibile trovare $hash(m_1)=hash(m_2)$.
\end{enumerate}
La resistenza alle collisioni non esclude che queste possano verificarsi in quanto lo spazio di output è limitato mentre quello in input è potenzialmente infinito: la proprietà implica solamente che trovare una collisione non deve essere computazionalmente fattibile.

Ad esempio per una funzione di hashing con un output di 256 bit è possibile trovare una collisione con una possibilità del $99.8\%$ calcolando $2^{130}+1$ (per la teoria del \textit{paradosso del compleanno}) stringhe in input; tuttavia, il calcolo di ogni $2^{130}$ possibilità risulta essere impossibile con l'attuale potenza computazionale\footnote{Calcolando $10 000 H/s$ sarebbero necessari più $10^{27}$ anni per calcolare $2^{128}$ hash.}.

La ricerca di collisoni con metodo di brute-force nel paradosso del compleanno ha un costo di $\sqrt{\pi/2} \cdot 2^{n/2}$ chiamate alla funzione di hashing.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.23]{./images/colliding-blocks.png}
    \caption{Blocchi dei messaggi in collisione}
\end{figure}

Questa proprietà permette quindi di ottenere dei \textit{digest} di un messaggio di lunghezza fissa e che sono univoci per il messaggio dato in input riducendo la quantità di informazioni da memorizzare e velocizzando i controlli di integrità ed uguaglianza.

In base a queste proprietà le funzioni di hashing sono anche dette \textit{one-way} in quanto non sono reversibili.
\\\\*
Esistono diverse classi di funzioni di hash crittografiche:
\begin{itemize}
    \item Secure Hashing Algorithm (\textit{SHA-1}, \textit{SHA-2} e \textit{SHA-3});
    \item RACE Integrity Primitive  Evaluation Message Digest (\textit{RIPEMD});
    \item Message Digest Algorithm 5 (\textit{MD5});
    \item BLAKE2
\end{itemize}

\begin{table}[h]
    \caption{Confronto tra \textit{SHA-1} di due stringhe simili}
    \centering
    \resizebox{\textwidth}{!}{\begin{tabular}{c||c}
            unibo & 20e6c59af7ebd8b3e61cb41496f4b2fd9e74098e \\
            \cline{1-2}
            unobo & ed6ff092453871627ea9841a6874aee6e6396a51 \\
        \end{tabular}}
\end{table}
Tramite le funzioni di hashing è possibile quindi rendere noto anche l'hash di un valore segreto in quanto questo non può essere dedotto. Ad esempio è possibile generate un hash \texttt{com} di \texttt{msg} ed un \texttt{nonce}; pubblicando l'hash \texttt{com} nessuno sarà in grado di risalire al valore di \texttt{msg}. Una volta resi pubblici \texttt{msg} e \texttt{nonce} allora sarà possibile verificare che \texttt{com} sia effettivamente l'hash reso pubblico in precedenza.\newline
Questa tecnica è utilizzata, ad esempio, per risalire alla paternità di brevetti: pubblicando \texttt{com} generato con in input alla funzione di hash il documento dell'invenzione (\texttt{msg}) ed un timestamp (\texttt{nonce}) sarà possibile, una volta reso pubblico il brevetto e la data, garantire che l'invenzione sia stata realizzata prima di un altra.
\newline\newline
Nel dettaglio del presente lavoro lo studio si concentra sull'algoritmo di hashing crittografico SHA-1.
